Welcome to ACoTE's documentation!
=================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   core
   memory
   classification
   consideration
   contemplation
   perception
   proposition
   reaction
   tutorials/instantiation
   tutorials/configuration
