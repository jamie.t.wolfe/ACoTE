import os
import sys
from acote.acote-core import controller, node

def main():
    if agent_config_check(AGENT_NAME):
        cotnc = controller.init()
        cotn = node.init(cotnc)
    else:
        print("No valid config")
        sys.exit(1)

def agent_config_check(agent_name):
    if check_folder_exists(agent_name)    

def check_folder_exists(folder_name):
    cwd = os.getcwd()
    if os.path.isdir(cwd):
        for root, dirs, files in os.walk(cwd):
            if folder_name in dirs:
                return True
    return False