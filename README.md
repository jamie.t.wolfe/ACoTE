# Agentic Chain of Thought Engine
# WIP - NOT FUNCTIONAL

## Overview
The goal of ACoTE is to create a series of 'cognitive' mechanisims that are implemented using modern AI tools to produce an informational structure that enable emulation of anthropomorphic features such as;
- Empathy and Sympathy - with the goal of providing context to a continium of interactions in a social manner.
- Personality and Autonomy - with the goal of abstracting the randomness away from 'Information Topologies' and into time dependent experience.
- Innovation and Adaptability - with the goal of enabling a process for self driven development of novel solutions to new problems.

## Engine Elements
ACoTE can be broken down so far into a few basic concepts.
- 1. Perception - A platform that can be host to any number of raw input methods (Text, Image, Audio, or any other format of data.)
- 2. Classification - An environment that classifies input data based on its relevance to data from the past and deviation from expected percieved data. 
- 3. Proposition - Given the classification of input data, what data is important (relevants to previous contemplation, reaction, or )
- 4. Contemplation - After generating a proposition of what to do with the classified data and whether it is important it should be considered of this new information should be acted on.
- 5. Reaction - Considering what information should be acted on what is the next data to be output.

## Implementation
From the Engine Elements there is a basic framework and parrallels that can be drawn with new AI tools (questions to be answered)
- 1. For a single time step all input models are queried to provide an 'internal perception map' (What was said, What did I see, What did I Read, What did I remember, etc).
- 2. Is the data that was recieved and computed by the host models relevant to what has happend previously and what sort of information is availible? Also to what level is this data important in continuing the chain of though from the previous time step.
- 3. What needs to be done with the data that has been classified by importance? Is there new information that is relevant to the process occurring in chain of thought?
- 4. Condidering the data that is proposed to be the most important data should I use any of it to tune my response or to alter my actions?
- 5. Generate a response in whatever modes 

## What does GPT3.5T say about this?

What is Agentic Chain of Thought?
```
The agentic chain of thought is a psychological theory that suggests that human behavior is primarily driven by an individual's perception of their ability to control and influence their environment. According to this theory, people who believe that they have control over their environment are more likely to take action and be proactive, while those who believe that their environment controls them are more likely to be passive and reactive.
The agentic chain of thought was developed by psychologist Albert Bandura, who also developed the concept of self-efficacy. Bandura believed that self-efficacy, or an individual's belief in their own ability to achieve their goals and succeed, was a crucial factor in determining behavior.
The agentic chain of thought posits that a person's self-efficacy is influenced by four key factors: personal factors (such as skills and abilities), environmental factors (such as the availability of resources and social support), behavioral factors (such as past experiences and successes), and cognitive factors (such as beliefs and values).
According to the agentic chain of thought, these factors work together in a chain-like fashion to influence an individual's behavior. For example, a person who believes that they have the skills and abilities necessary to achieve their goals is more likely to seek out resources and social support to help them do so. In turn, this support can help boost their confidence and self-efficacy, leading them to take more proactive steps towards achieving their goals.
```
Summarise the internal processes that a human would experience when taking in, evaluating and then responding to information in the context of LLM Agents, memory tools (like vector DB), langchain tools, etc.
```
When a human takes in information in the context of LLM Agents, memory tools like vector DB, and langchain tools, they typically go through several internal processes. These processes include:
Perception: The human senses receive information from the environment through various channels, such as visual, auditory, and tactile.
Attention: The human brain selects which information to focus on and process further, based on its relevance and importance.
Encoding: The human brain converts the information into a form that can be stored and processed later, such as through language, visual imagery, or other sensory representations.
Storage: The human brain stores the encoded information in memory, either in short-term or long-term memory.
Retrieval: When the human brain needs to recall the information, it retrieves it from memory and brings it back into conscious awareness.
Evaluation: The human brain evaluates the information based on its relevance, credibility, and coherence with other knowledge and beliefs.
Integration: The human brain integrates the new information with existing knowledge and beliefs to form a coherent mental representation of the world.
Response: The human brain generates a response based on the integrated information, which may include cognitive, emotional, or behavioral reactions.
Overall, these internal processes are essential for humans to make sense of the complex and ever-changing world around them, and to adapt and respond effectively to new information and challenges.
```

## Installation
Clone this bad boy and go nuts. It can be built to freeze the state of everything so that the 'personality' and functionality of the 'hyper-model' remains 'stable'.

## Usage
- Program can be built with `build.py` in 'server', 'client' or 'standalone' mode.
- Program can be executed with 'main.py' in 'server', 'client' or 'standalone' mode.

A built server exe is recommended, the api can then be used to interact with it via a web ui (wui not currently considered)

## Support
Just post an issue. I'll see it.

## Roadmap
- [x] Template the project a bit.
- [x] Template the project some more.
- [ ] Implement text chat with MM memory and basic tool use
- [ ] Convert process in text to a generic
- [ ] Template tool use to a generic
- [ ] Prototype Image and Audio Cot inputs from generic
- [ ] Extend tool use from generic
- [ ] Create test architecure for Agentic MM inputs
- [ ] Build API
- [ ] Build Interface (multi-user chat room)

## Contributing
Not sure if anyone will ever want to help with this. I dunno, create an Issue?

## Authors and acknowledgment
Me

## License
Apache 2 I guess, I'm  not a lawyer. Please let me know if you want to commercialise this tho, the goal is to create an agentic AI that could be considered to have personage and so be the recipient of some type of rights. I guess licensing such a thing might be considered slavery then? 

## Project status
Active - But I have a full time job so unless they want to pay me to make a brain baby 'active' is more of a vague suggestion.
