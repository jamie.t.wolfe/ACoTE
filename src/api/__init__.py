# my_program/database/__init__.py

# Import modules
from .connection import connect
from .models import User, Order

__all__ = ['connect', 'User', 'Order']
