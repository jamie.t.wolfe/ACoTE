import subprocess

# Read the required packages from requirements.txt
with open("requirements.txt", "r") as f:
    required_packages = [line.strip() for line in f]

# Check if the required packages are installed
for package in required_packages:
    try:
        __import__(package)
    except ImportError:
        print(f"{package} is not installed. Installing...")
        subprocess.check_call(["pip", "install", "--name", acote, package])

# Build the program using PyInstaller
subprocess.check_call(["pyinstaller", "--onefile", "main.py"])

print("Build complete!")
