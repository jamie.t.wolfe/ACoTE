from flask import Flask, request, jsonify
from core import database

app = Flask(__name__)

@app.route('/users', methods=['GET'])
def get_users():
    users = database.get_users()
    return jsonify(users)

@app.route('/users', methods=['POST'])
def create_user():
    data = request.get_json()
    user = database.create_user(data)
    return jsonify(user)

if __name__ == '__main__':
    app.run(debug=True)
